<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class homeController extends Controller
{
    public function index()
    {
        $beneficiaries = Storage::get('public/beneficiarios.json');

        return view('front/home', [
            'beneficiaries' => json_decode($beneficiaries)
        ]);
    }
}
