<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class BeneficiariesController extends Controller
{
    public function beneficiaries(Request $request)
    {
        $beneficiaries = $request->beneficiarios;
        $plans = $this->plans();
        $prices = $this->prices();
        $jsonBeneficieries = [];
        $total = 0;

        // Faixa 1 0 a 17
        // Faixa 2 18 a 40
        // Faixa 3 +40 anos

        foreach ($plans as $plan) {
            if ($plan->codigo == $beneficiaries['plano']['codigo']) {

                foreach ($prices as $price) {
                    if ($price->codigo == $beneficiaries['plano']['codigo'] &&
                    $beneficiaries['quantidade'] >= $price->minimo_vidas) {
                        foreach ($beneficiaries['beneficiario'] as $beneficiarie) {
                            if ($beneficiarie['idade'] > 0 && $beneficiarie['idade'] <= 17) {
                                $pricePlan = $price->faixa1;
                                $total += $pricePlan;
                                $array = [
                                    "nome" => $beneficiarie['nome'],
                                    "idade" => $beneficiarie['idade'],
                                    "plano" => [
                                        "nome" => $plan->nome,
                                        "preco" => $pricePlan
                                    ]
                                ];
                                array_push($jsonBeneficieries, $array);
                            }

                            if ($beneficiarie['idade'] > 18 && $beneficiarie['idade'] <= 40) {
                                $pricePlan = $price->faixa2;
                                $total += $pricePlan;
                                $array = [
                                    "nome" => $beneficiarie['nome'],
                                    "idade" => $beneficiarie['idade'],
                                    "plano" => [
                                        "nome" => $plan->nome,
                                        "preco" => $pricePlan
                                    ]
                                ];
                                array_push($jsonBeneficieries, $array);
                            }

                            if ($beneficiarie['idade'] > 41) {
                                $pricePlan = $price->faixa3;
                                $total += $pricePlan;
                                $array = [
                                    "nome" => $beneficiarie['nome'],
                                    "idade" => $beneficiarie['idade'],
                                    "plano" => [
                                        "nome" => $plan->nome,
                                        "preco" => $pricePlan
                                    ]
                                ];
                                array_push($jsonBeneficieries, $array);
                            }

                        }
                    }
                }

                $json = [
                    "codigo_plano" => $beneficiaries['plano']['codigo'],
                    "mensagem" => "Sucesso!",
                    "beneficiario" => $jsonBeneficieries,
                    "total" => $total
                ];
                break;
            } else {
                $json = [
                    "codigo" => "0",
                    "mensagem" => "Plano escolhido não existe!"
                ];
            }
        }

        Storage::disk('public')->put('beneficiarios.json', response()->json($json)->getContent());
        return response()->json($json);
    }

    public function plans()
    {
        $json = json_decode(file_get_contents('storage/json/plans.json'));
        return $json;
    }

    public function prices()
    {
        $json = json_decode(file_get_contents('storage/json/prices.json'));
        return $json;
    }
}
