<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Home | Beneficiarios</title>

  <link rel="stylesheet" href="storage/css/style.css">
</head>
<body>
  <div class="container">
    <h2>Lista de Beneficiários</h2>
    <div class="row">
      @foreach ($beneficiaries->beneficiario as $beneficiarie)
        <ul>
          <li>Beneficiária</li>
          <li>Nome: {{ $beneficiarie->nome }}</li>
          <li>Idade: {{ $beneficiarie->idade }} anos</li>
          <ul>
            <li>Plano Selecionado: <strong>{{ $beneficiarie->plano->nome }}</strong> </li>
            <li>Preço individual: <strong>R$ {{ number_format($beneficiarie->plano->preco, 2, ',', '.') }}</strong></li>
          </ul>
        </ul>
      @endforeach
    </div>
    <h3>Valor Total: R$ {{ number_format($beneficiaries->total, 2, ',', '.') }}</h3>
  </div>
</body>
</html>
