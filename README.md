
![Logo](https://www.planium.io/wordpress/wp-content/uploads/2018/11/logo-Planium-06.svg)


# Documento

Leia o documento para entender melhor a instalação e uso da aplicação


## Instalação

Clone o projeto
```bash
  git clone https://gitlab.com/moisesfausto/planium.git
```

Acesse a pasta do projeto
```bash
  cd planium
```

Copie o arquivo .env.example
```bash
  cp .env.example .env
```

Atualize ou instale o Composer e aguarde finalizar
```bash
  composer update
```

Gere uma nova Key
```bash
  php artisan key:generate
```



## Rodar localmente

Criar um link simbólico para a pasta /public
```bash
  php artisan storage:link
```

Rodar o servidor interno da aplicação
```bash
  php artisan serve
```





## Como usar

Endpoint:
```bash
  http://127.0.0.1:8000/api/beneficiarios
```

Enviar via POST uma estrutura análoga a de baixo
```javascript
{
  "beneficiarios": {
    "quantidade": 3,
    "beneficiario": [{
        "nome": "Fatima",
        "idade": 29
      },
      {
        "nome": "Mariclide",
        "idade": 56
      },
      {
        "nome": "Josivalda",
        "idade": 6
      }
    ],
    "plano": {
      "codigo": 1
    }
  }
}
```
## Frontend

Para acessar o visual do arquivo que foi gerado com as informações do beneficiarios, acesse o link:
```bash
  http://127.0.0.1:8000/
```
